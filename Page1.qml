import QtQuick 2.0
import QtQuick.Controls 6.2

Item {
    id: item1
    Text {
        id: text1
        x: 158
        y: 26
        width: 138
        height: 37
        color: "#000000"
        text: qsTr("Text Field")
        elide: Text.ElideNone
        font.pixelSize: 30
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        wrapMode: Text.NoWrap
        renderType: Text.QtRendering
        font.underline: true
        font.bold: true
        font.weight: Font.DemiBold
        font.family: "Times New Roman"
        clip: true
    }

    Button {
        id: button
        x: 335
        y: 38
        text: qsTr("Press Me")
        font.family: "Times New Roman"
    }

    TextArea {
        id: textArea
        width: 502
        height: 216
        anchors.verticalCenter: parent.verticalCenter
        wrapMode: Text.WordWrap
        font.weight: Font.ExtraLight
        anchors.horizontalCenter: parent.horizontalCenter
        layer.enabled: true
        textFormat: Text.AutoText
        clip: true
        placeholderText: qsTr("Text Area")
    }

}

/*##^##
Designer {
    D{i:0;height:480;width:640}D{i:1;annotation:"1 //;;//  //;;//  //;;// <!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\np, li { white-space: pre-wrap; }\n</style></head><body style=\" font-family:'Ubuntu'; font-size:11pt; font-weight:400; font-style:normal;\">\n<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Tibetan Machine Uni'; font-weight:700; text-decoration: underline;\">Text Field</span></p></body></html> //;;// 1647123483";customId:""}
D{i:3}
}
##^##*/
